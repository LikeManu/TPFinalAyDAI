#ifndef POLIGONO_H
#define POLIGONO_H

#include <QMainWindow>
#include <QtGui>
#include <QtCore>

namespace Ui {
class Poligono;
}

class Poligono : public QMainWindow
{
    Q_OBJECT

public:
    explicit Poligono(QWidget *parent = nullptr);
    ~Poligono();

private slots:
    void on_ADDbutton_clicked();

    void on_POLIGONObutton_clicked();

    void on_DELETEbutton_clicked();

    void on_TRIANGLEbutton_clicked();

    void wheelEvent(QWheelEvent * event);

    void mouseMoveEvent(QMouseEvent *event);

    void mousePressEvent(QMouseEvent *event);

    void mouseReleaseEvent(QMouseEvent *event);

    void on_HSbutton_clicked();

    void closeEvent (QCloseEvent *event);

private:
    Ui::Poligono *ui;
    void paintEvent (QPaintEvent *e);
    bool flag = false;
    float escala = 1;
    bool mouse_presionado = false;
    int posx,pos1x,posy,pos1y;
};

#endif // POLIGONO_H
