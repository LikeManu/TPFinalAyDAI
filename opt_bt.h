#ifndef OPT_BT_H
#define OPT_BT_H

#include <QDialog>
#include <arbinbus.h>
#include <node.h>

namespace Ui {
class opt_bt;
}

class opt_bt : public QDialog
{
    Q_OBJECT

public:
    explicit opt_bt(QWidget *parent = nullptr);
    ~opt_bt();

private slots:

    void on_sb_cantPalabras_valueChanged(int arg1);

    void on_table_cellChanged(int row, int column);

    void on_b_calcular_clicked();

    void on_b_archivo_clicked();

    void closeEvent (QCloseEvent *event);

private:
    Ui::opt_bt *ui;

    struct node{
        int pos;
        double val;};

    node** inicMat(int i);

    double sum(short int i,short int j,short int a);

    double min(short int i, short int j, short int& k, node** mat);

    void fillMat(node** mat);

    void fillArbinMat(arbinbus<QString>& arbin,node** mat, int i, int j);

    void dibujarArbin(arbinbus<QString> arbin,Node *node,int i);
};

#endif // OPT_BT_H
