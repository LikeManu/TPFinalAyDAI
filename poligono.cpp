#include "poligono.h"
#include "ui_poligono.h"
#include "math.h"
#include <QPoint>
#include <QtWidgets>
#include "par.h"

const int tamanio = 60;

struct Polin{
    Par pares[tamanio];
    int cantidad;
};

struct Nodo{
    float distancia;
    int pos;
};

float Distancia(int i, int j, Polin p){
    float res;
    if ((i+1 == j) or (i-1 == j)) {
        res = 0;}
    else{
        res = sqrt(pow((p.pares[j].obtenerx() - p.pares[i].obtenerx()), 2) + (pow((p.pares[j].obtenery() - p.pares[i].obtenery()), 2)));}
    return res;
}

//Si el valor llega al final del Poligono, volvera al V inicial.
int valorReducido(int i, int k, Polin pol){
    while (k != 0){
        i++;
        if (i == pol.cantidad)
            i = 0;
        k--;
    }
    return i;
}

//Esta funcion analiza distintos casos para evitar que se termine accediendo a valores de la Matriz que no existen.
float obtminimo (int i, int k, int s, Polin pol, Nodo matriz[][tamanio]){
    float n;
    n = matriz[i][k+1].distancia;

    if ((i+k) >= (pol.cantidad))
        n = n + matriz[valorReducido(i, k, pol)][s-k].distancia;
    else
        n = n + matriz[i+k][s-k].distancia;

    if ((i+k) >= (pol.cantidad))
        n = n + Distancia(i,valorReducido(i, k, pol), pol);
    else
        n = n + Distancia(i, i+k, pol);

    if (((i+k) >= (pol.cantidad)) and (i+s-1 >= pol.cantidad))
        n = n + Distancia(valorReducido(i, k, pol), valorReducido(i, s-1, pol), pol);
    if (((i+k) >= (pol.cantidad)) and (i+s-1 < pol.cantidad))
        n = n + Distancia(valorReducido(i, k, pol), i+s-1, pol);
    if (((i+k) < (pol.cantidad)) and (i+s-1 >= pol.cantidad))
        n = n + Distancia(i+k, valorReducido(i, s-1, pol), pol);
    if (((i+k) < (pol.cantidad)) and (i+s-1 < pol.cantidad))
        n = n + Distancia(i+k, i+s-1, pol);

    return n;
}

float minimo(int i, int &k, int s, Polin pol, Nodo matriz[][tamanio]){
    k = 1;
    int kmin= 1;
    float m, n;
    m = 0;
    while (k <= s-2) {
        n = obtminimo(i, k, s, pol, matriz);
        if (k == 1){
            m = n;}
        else{
            if (n < m){
                m = n;
                kmin = k;}
        }
        k++;
    }
    k = kmin;
    return m;
}

void cargarPares(Polin &pol, int x, int y){
    Par a;
    a.agregar(x, y);
    pol.pares[pol.cantidad] = a;
    pol.cantidad++;
 }

void obtenerTriangulizacion(int i, int j, Polin pol, Polin &triangulo, Nodo matriz[][tamanio]){
    if (j >= 4){
        int k;
        k = matriz[i][j].pos;
        int x, y;
        if (k != 1){
            x = pol.pares[i].obtenerx();
            y = pol.pares[i].obtenery();
            cargarPares(triangulo, x, y);
            x = pol.pares[i+k].obtenerx();
            y = pol.pares[i+k].obtenery();
            cargarPares(triangulo, x, y);
        }
        if (k != j-2){
            x = pol.pares[i+k].obtenerx();
            y = pol.pares[i+k].obtenery();
            cargarPares(triangulo, x, y);
            x = pol.pares[i+j-1].obtenerx();
            y = pol.pares[i+j-1].obtenery();
            cargarPares(triangulo, x, y);
        }
        obtenerTriangulizacion(i, k+1, pol, triangulo, matriz);
        obtenerTriangulizacion(i+k, j-k, pol, triangulo, matriz);
    }
}

using namespace std;

Poligono::Poligono(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Poligono)
{
    ui->setupUi(this);
    posx = width()/2;
    posy = height()/2;
}

Poligono::~Poligono()
{
    delete ui;
}

void Poligono::on_ADDbutton_clicked()
{
    if (ui->ADDbutton->styleSheet() != "background-color: red")
    ui->tableWidget->setRowCount(ui->tableWidget->rowCount()+1);
}

void Poligono::on_DELETEbutton_clicked()
{
    if (ui->DELETEbutton->styleSheet() != "background-color: red"){
        if(ui->tableWidget->currentRow() == -1)
            ui->tableWidget->setRowCount(ui->tableWidget->rowCount()-1);
        else
            ui->tableWidget->removeRow(ui->tableWidget->currentRow());}
}

void Poligono::paintEvent (QPaintEvent *e){

    QPainter painter (this);

    painter.setRenderHint(QPainter::Antialiasing);
    painter.scale(escala,escala);
    painter.translate(posx, posy);

    QPen linepen (Qt::black);
    linepen.setWidth(2);

    QPen pointpen (Qt::red);
    pointpen.setWidth(6);

    QPen trianglepen (Qt::green);
    pointpen.setWidth(2);

    painter.setPen(pointpen);

    int i = 0;
    int max = ui->tableWidget->rowCount();

    Polin pol;
    pol.cantidad= 0;

    bool ok,ok1;
    int x, y;
    //Carga todos los datos en Pol para facilitar su uso.
    while (i <= (max-1)){
      ok = true;
      ok1 = true;
      x = ui->tableWidget->model()->index(i, 0).data().toInt(&ok);
      y = ui->tableWidget->model()->index(i, 1).data().toInt(&ok1);
      if(ok && ok1)
        cargarPares(pol, x, y);
      else
          max--;
      i++;}

    QPoint p1;
    QPoint p2;

    i = 0;

    //Dibuja las lineas del Poligono.
    painter.setPen(linepen);
    if (pol.cantidad != 0){
        if (pol.cantidad != 1){
            while ((i+1) <= (max-1)){

                p1.setX(pol.pares[i].obtenerx());
                p1.setY(pol.pares[i].obtenery());

                p2.setX(pol.pares[i+1].obtenerx());
                p2.setY(pol.pares[i+1].obtenery());

                painter.drawLine(p1, p2);
                i++;
            }
            p1.setX(pol.pares[0].obtenerx());
            p1.setY(pol.pares[0].obtenery());
            painter.drawLine(p1, p2);}

        //Dibuja los puntos del Poligono.
        i = 0;
        painter.setPen(pointpen);
        while (i <= (max-1)){
            p1.setX(pol.pares[i].obtenerx());
            p1.setY(pol.pares[i].obtenery());
            painter.drawPoint(p1);
            i++;
        }
     }

    //Solo al presionar el boton TRIANGULATION.
    if (flag){
        i = 0;
        int j = 0;
        int k = 0;
        Nodo matriz[tamanio][tamanio];
        while ((j <= 3) and (j <= pol.cantidad)){
            while (i <= (pol.cantidad-1)){
                matriz[i][j].distancia = 0;
                matriz[i][j].pos = k;
                i++;
            }
            j++;
            i=0;
        }
        j = 4;
        while (j <= (pol.cantidad)) {
            i = 0;
            if (j != pol.cantidad){
            while (i <= pol.cantidad-1){
                matriz[i][j].distancia = minimo(i, k, j, pol, matriz);
                matriz[i][j].pos = k;
                i++;
                }
            }
            else{
                matriz[i][j].distancia = minimo(i, k, j, pol, matriz);
                matriz[i][j].pos = k;
            }
            j++;
        }

        i = 0;
        j = pol.cantidad;
        Polin triangulo;
        triangulo.cantidad = 0;
        //Obtiene los datos de la triangulizacion.
        obtenerTriangulizacion(i, j, pol, triangulo, matriz);

        //Se dibuja la triangulizacion en pantalla.
        painter.setPen(trianglepen);
        while (i < triangulo.cantidad){
            p1.setX(triangulo.pares[i].obtenerx());
            p1.setY(triangulo.pares[i].obtenery());
            p2.setX(triangulo.pares[i+1].obtenerx());
            p2.setY(triangulo.pares[i+1].obtenery());

            painter.drawLine(p1, p2);
            i++;
            i++;
        }

        i=0;
        painter.setPen(pointpen);
        while (i < (triangulo.cantidad)){
            p1.setX(triangulo.pares[i].obtenerx());
            p1.setY(triangulo.pares[i].obtenery());
            painter.drawPoint(p1);
            i++;
        }

        ui->labelCosto->setText("Costo optimo:");
        ui->COSTO->setText(QString::number(matriz[0][pol.cantidad].distancia));

    }


}

void Poligono::on_POLIGONObutton_clicked()
{
    if (ui->POLIGONObutton->styleSheet() != "background-color: red"){
    flag = false;
    ui->TRIANGLEbutton->setStyleSheet("background-color: green");
    update();}
}

void Poligono::on_TRIANGLEbutton_clicked()
{
    if (ui->TRIANGLEbutton->styleSheet() != "background-color: red"){
    flag = true;
    update();
    ui->TRIANGLEbutton->setStyleSheet("background-color: red");}}

void Poligono::wheelEvent(QWheelEvent *event)
{
    if(event->delta() > 1)
        escala= escala + 0.1;
    else
        escala= escala - 0.1;
    update();
}

void Poligono::mousePressEvent(QMouseEvent *event){
    mouse_presionado = true;
    pos1x = event->x();
    pos1y = event->y();
}

void Poligono::mouseReleaseEvent(QMouseEvent *event){
    mouse_presionado = false;
}

void Poligono::mouseMoveEvent(QMouseEvent *event){
    if(mouse_presionado){
    posx = posx + (event->x() - pos1x);
    pos1x = event->x();
    posy = posy + (event->y() - pos1y);
    pos1y = event->y();
    update();}
}

void Poligono::on_HSbutton_clicked()
{
    if (ui->HSbutton->styleSheet() != "background-color: red"){
        ui->ADDbutton->setVisible(true);
        ui->DELETEbutton->setVisible(true);
        ui->POLIGONObutton->setVisible(true);
        ui->TRIANGLEbutton->setVisible(true);
        ui->tableWidget->setVisible(true);
        ui->HSbutton->setStyleSheet("background-color: red");
        ui->HSbutton->setText("HIDE ALL");
    }
    else{
        ui->ADDbutton->setVisible(false);
        ui->DELETEbutton->setVisible(false);
        ui->POLIGONObutton->setVisible(false);
        ui->TRIANGLEbutton->setVisible(false);
        ui->tableWidget->setVisible(false);
        ui->HSbutton->setStyleSheet("background-color: green");
        ui->HSbutton->setText("SHOW ALL");
    }
}

void Poligono::closeEvent (QCloseEvent *event){
    QMessageBox::StandardButton resBtn = QMessageBox::question(this, "Atención", tr("¿Está seguro de que desea salir?\n"), QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes, QMessageBox::Yes);
    if (resBtn != QMessageBox::Yes)
        event->ignore();
    else
        event->accept();}
