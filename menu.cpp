#include <menu.h>
#include <ui_menu.h>
#include <QMessageBox>

menu::menu(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::menu)
{
    ui->setupUi(this);
}

menu::~menu()
{
    delete ui;
}

void menu::on_b_poli_pressed(){
    form_poli = new Poligono();
    form_poli->show();}

void menu::on_b_bt_pressed(){
    form_bt = new opt_bt();
    form_bt->show();}

void menu::on_b_exit_pressed(){
    this->close();}

void menu::on_b_mat_pressed(){
    form_mat = new MainWindow();
    form_mat->show();}

void menu::closeEvent (QCloseEvent *event){
    QMessageBox::StandardButton resBtn = QMessageBox::question(this, "Atención", tr("¿Está seguro de que desea salir?\n"), QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes, QMessageBox::Yes);
    if (resBtn != QMessageBox::Yes)
        event->ignore();
    else
        event->accept();}
