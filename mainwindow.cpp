#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "matrizoptima.h"
#include <QMessageBox>
#include <QCloseEvent>
#include <ayuda.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->spinBox_3->setEnabled(false);
    ui->spinBox_5->setEnabled(false);
    ui->spinBox_7->setEnabled(false);
    ui->pushButton_3->setEnabled(false);
    ui->pushButton_4->setEnabled(false);
    QPalette palette;
    palette.setColor(ui->label->foregroundRole(), Qt::white);
    ui->label->setPalette(palette);
    ui->label_12->setPalette(palette);
    ui->label_13->setPalette(palette);
    ui->label_14->setPalette(palette);
    ui->label_15->setPalette(palette);
    ui->label->setAttribute(Qt::WA_TranslucentBackground);
    ui->label_12->setAttribute(Qt::WA_TranslucentBackground);
    ui->label_13->setAttribute(Qt::WA_TranslucentBackground);
    ui->label_14->setAttribute(Qt::WA_TranslucentBackground);
    ui->label_15->setAttribute(Qt::WA_TranslucentBackground);
}

MainWindow::~MainWindow()
{
    delete ui;
}

//Spin box de las filas de la matriz 1
void MainWindow::on_spinBox_valueChanged(const int &arg1)
{
    ui->tableWidget->setRowCount(arg1);
    ui->pushButton_3->setEnabled(false);
    ui->pushButton_4->setEnabled(false);
}

//Spin box de las columnas de la matriz 1
void MainWindow::on_spinBox_2_valueChanged(const int &arg1)
{
    ui->tableWidget->setColumnCount(arg1);
    ui->tableWidget_2->setRowCount(arg1);
    ui->spinBox_3->setValue(arg1);
    ui->pushButton_3->setEnabled(false);
    ui->pushButton_4->setEnabled(false);
}

//Spin box de las columnas de la matriz 2
void MainWindow::on_spinBox_4_valueChanged(const int &arg1)
{
    ui->tableWidget_2->setColumnCount(arg1);
    ui->tableWidget_3->setRowCount(arg1);
    ui->spinBox_5->setValue(arg1);
    ui->pushButton_3->setEnabled(false);
    ui->pushButton_4->setEnabled(false);
}

//Spin box de las columnas de la matriz 3
void MainWindow::on_spinBox_6_valueChanged(const int &arg1)
{
    ui->tableWidget_3->setColumnCount(arg1);
    ui->tableWidget_4->setRowCount(arg1);
    ui->spinBox_7->setValue(arg1);
    ui->pushButton_3->setEnabled(false);
    ui->pushButton_4->setEnabled(false);
}

//Spin box de las columnas de la matriz 4
void MainWindow::on_spinBox_8_valueChanged(const int &arg1)
{
     ui->tableWidget_4->setColumnCount(arg1);
     ui->pushButton_3->setEnabled(false);
     ui->pushButton_4->setEnabled(false);
}

//Botón Cantidad óptima de multiplicaciones, cuando se cliquea esta opción se almacenan las dimensiones de las matrices en un arreglo y se llama a los procedimientos encargados de calcular la cantidad óptima de multiplicaciones requeridas.
void MainWindow::on_pushButton_2_clicked()
{
    float valor;
    QString optimo;
    this->dimMat[0] = ui->spinBox->value();
    this->dimMat[1] = ui->spinBox_2->value();
    this->dimMat[2] = ui->spinBox_4->value();
    this->dimMat[3] = ui->spinBox_6->value();
    this->dimMat[4] = ui->spinBox_8->value();
    valor = this->matriz->armaMatriz(this->dimMat);
    optimo= (QString::number(valor));
    ui->label_10->setText(optimo);
    ui->pushButton_3->setEnabled(true);
    ui->pushButton_4->setEnabled(true);
    ui->label_11->setText("");
}

void MainWindow::on_pushButton_3_clicked()
{
    std::string orden;
    this->matriz->ordenDeMultiplicacion(0,cantMat - 1, orden);
    ui->label_11->setText(QString::fromStdString(orden));
}

//Botón Multiplicar matrices, cuando se cliquea esta opción se multiplican las 4 matrices y se almacena el resultado en la tabla de arriba del botón.
void MainWindow::on_pushButton_4_clicked()
{
    this->matrices[0] = this->matriz->cargarMatriz(this->dimMat[0], this-> dimMat[1], ui->tableWidget);
    this->matrices[1] = this->matriz->cargarMatriz(this->dimMat[1], this-> dimMat[2], ui->tableWidget_2);
    this->matrices[2] = this->matriz->cargarMatriz(this->dimMat[2], this-> dimMat[3], ui->tableWidget_3);
    this->matrices[3] = this->matriz->cargarMatriz(this->dimMat[3], this-> dimMat[4], ui->tableWidget_4);
    this->matrizRes = this->matriz->multiplicarMatrices(this->matrices,this->dimMat,0,cantMat - 1);
    this->matriz->cargarTabla(this->dimMat[0],this->dimMat[4],ui->tableWidget_5,this->matrizRes);
}

//Opción Ayuda del menú de herramientas, cuando se elige esta opción se despliega el formulario ayuda, donde se explica el fin y como funciona el programa.
void MainWindow::on_actionAyuda_triggered()
{
    a = new Ayuda();
    a->show();
}

//Opción Cerrar del menú de herramientas, cuando se elige esta opción se le pregunta al usuario si esta seguro si desea cerrar o no el programa.
void MainWindow::on_actionCerrar_triggered()
{
    close();
}

//Opción Limpiar pantalla del menú de herramientas, cuando se elige esta opción se reinician los valores a los que tenia en un principio.
void MainWindow::on_actionLimpiar_Pantalla_triggered()
{
    ui->spinBox->setValue(0);
    ui->spinBox_2->setValue(0);
    ui->spinBox_3->setValue(0);
    ui->spinBox_4->setValue(0);
    ui->spinBox_5->setValue(0);
    ui->spinBox_6->setValue(0);
    ui->spinBox_7->setValue(0);
    ui->spinBox_8->setValue(0);
    ui->tableWidget->defaultDropAction();
    ui->tableWidget_2->defaultDropAction();
    ui->tableWidget_3->defaultDropAction();
    ui->tableWidget_4->defaultDropAction();
    ui->tableWidget_5->clear();
    ui->tableWidget_5->setColumnCount(0);
    ui->tableWidget_5->setRowCount(0);
    ui->pushButton_3->setEnabled(false);
    ui->pushButton_4->setEnabled(false);
    ui->label_10->setText("");
    ui->label_11->setText("");
}

void MainWindow::closeEvent (QCloseEvent *event)
{
    QMessageBox::StandardButton resBtn = QMessageBox::question(this, "Atención", tr("¿Está seguro de que desea salir?\n"), QMessageBox::Cancel | QMessageBox::No | QMessageBox::Yes, QMessageBox::Yes);
    if (resBtn != QMessageBox::Yes) {
        event->ignore();
    } else {
        event->accept();
    }
}
