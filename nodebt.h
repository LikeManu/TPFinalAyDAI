#ifndef NODEBT_H
#define NODEBT_H

template <typename T>
class nodebt{ //Clase nodobt
    public:
        T elem;
        nodebt *l;
        nodebt *r;
        nodebt(T elem){ //Constructora de la clase nodobt
            (this-> elem) = elem;
            this-> l = nullptr;
            this-> r = nullptr;}};

#endif // NODEBT_H
