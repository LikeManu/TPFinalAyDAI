#ifndef PAR_H
#define PAR_H


class Par
{
    public:
        Par(){
            this -> x = 0;
            this -> y = 0;
        }
        virtual ~Par(){
        }
        void agregar(int x, int y);
        int obtenerx();
        int obtenery();
        void mostrar();

    protected:

    private:
        int x;
        int y;
};

//Implementacion de Par<T>
void Par::agregar(int x, int y){
    this->x = x;
    this->y = y;
}

int Par::obtenerx(){
    return this -> x;
}

int Par::obtenery(){
    return this -> y;
}
#endif // PAR_H
