#ifndef MENU_H
#define MENU_H

#include <QDialog>
#include <opt_bt.h>
#include <poligono.h>
#include <mainwindow.h>

namespace Ui {
class menu;
}

class menu : public QDialog
{
    Q_OBJECT

public:
    explicit menu(QWidget *parent = nullptr);
    ~menu();

private slots:

    void on_b_poli_pressed();

    void on_b_bt_pressed();

    void on_b_exit_pressed();

    void on_b_mat_pressed();

    void closeEvent (QCloseEvent *event);

private:
    Ui::menu *ui;
    opt_bt *form_bt;
    Poligono *form_poli;
    MainWindow *form_mat;
};

#endif // MENU_H
