#ifndef MATRIZOPTIMA_H
#define MATRIZOPTIMA_H
#include <iostream>
#include <QTableWidget>
#include "math.h"
using namespace std;

const int cantMat = 4;

template <typename T>

class MatrizOptima
{
public:
    MatrizOptima();

    T **devolver_matrizOp();

    T **devolver_matrizk();

    T **inicMatriz(int limFila, int limCol, T valor);

    T **cargarMatriz(int limFila, int limCol, QTableWidget *tabla);

    void mostrarMatriz(T **matriz, T limFila, T limCol);

    void mostrarDim(T dimMat[], int tamanio);

    T minimo(int i, int j, T dimMat[]);

    T armaMatriz(T dimMat[]);

    T **multiplicarMatriz(T **matriz1, T **matriz2, T limFila1, T limCol1, T limCol2);

    T **multiplicarMatrices(T **matrices[], T dimMat[], int i, int j);

    void cargarTabla(int limFila, int limCol, QTableWidget *tabla, T ** matriz);

    void ordenDeMultiplicacion(int i, int j, std::string &orden);

private:
    T ** matrizOp;

    T ** matrizk;

};

template <typename T> MatrizOptima<T>::MatrizOptima()
{
    this ->matrizOp = inicMatriz(cantMat, cantMat,0);
    this ->matrizk = inicMatriz(cantMat, cantMat,0);
}

template <typename T> T **MatrizOptima<T>::devolver_matrizOp(){
    return this->matrizOp;
}

template <typename T> T **MatrizOptima<T>::devolver_matrizk(){
    return this->matrizk;
}

//Método que devuelve una matriz cargada con 0.
template <typename T> T **MatrizOptima<T>::inicMatriz(int limFila, int limCol, T valor){
    T **matriz = new T*[limFila];   //Reservando memoria para las filas
    for (int i = 0; i < limFila ; i++)
        matriz[i] = new T[limCol];    //Reservando memoria para las columnas
    for (int f = 0; f < limFila ; f++){
        for (int c = 0; c < limCol ; c++)
             *(*(matriz+f)+c) = valor;
    }
    return matriz;
}

//Método que devuelve una matriz cargada con los valores que el usuario ingreso en la tabla del formulario mainwindow.
template <typename T> T ** MatrizOptima<T>::cargarMatriz(int limFila, int limCol, QTableWidget *tabla){
    T **matriz = new T*[limFila];   //Reservando memoria para las filas
    for (int i = 0; i < limFila; i++)
        matriz[i] = new T[limCol];    //Reservando memoria para las columnas
    for (int f = 0; f < limFila; f++){
        for (int c = 0; c < limCol; c++){
            *(*(matriz+f)+c) = tabla->model()->index(f,c).data().toFloat();
        }
    }
    return matriz;
}

//Método que muestra una matriz por pantalla.
template <typename T> void MatrizOptima<T>::mostrarMatriz(T **matriz, T limFila, T limCol){
    cout << endl;
    for (int f = 0; f < limFila; f++){
        for (int c = 0; c < limCol; c++)
             cout << *(*(matriz+f)+c) << "       ";
        cout << endl;
    }
}

//Método que muestra un arreglo por pantalla.
template <typename T> void MatrizOptima<T>::mostrarDim(T dimMat[], int tamanio){
    cout << endl;
    for(int i = 0; i < tamanio ; i++){
        cout << dimMat[i] << endl;;
    }
}

//Método que devuelve el mínimo entre los valores requeridos de la matriz óptima.
template <typename T> T MatrizOptima<T>::minimo(int i, int j, T dimMat[]){
    T aux;
    T Min = INT_MAX;
    for (int k = i; k < j ; k++){
        aux = *(*(this->matrizOp+i)+k) + *(*(this->matrizOp+(k+1))+j) + (dimMat[i] * dimMat[k+1] * dimMat[j+1]);
        if(aux < Min){
            Min = aux;
            *(*(this->matrizk+i)+j) = k;
        }
    }
    return Min;
}

//Método que calcula la cantidad de multiplicaciones óptimas requeridas al multiplicar un grupo de matrices
template <typename T> T MatrizOptima<T>::armaMatriz(T dimMat[]){
    int i;
    int j;
    for (int diagonal = 1; diagonal < cantMat ; diagonal++){
        for (i = 0 ; i < (cantMat - diagonal) ; i++){
            j = i + diagonal;
            *(*(this->matrizOp+i)+j) = minimo(i, j, dimMat);
        }
    }
    return *(*(this->matrizOp+0)+cantMat-1);
}

template <typename T> T **MatrizOptima<T>::multiplicarMatriz(T **matriz1, T **matriz2, T limFila1, T limCol1, T limCol2){
    T  ** matrizRes = inicMatriz(limFila1,limCol2,0);
    for(int i = 0; i < limFila1; i++){
        for(int j = 0; j < limCol2; j++){
            for(int k = 0; k < limCol1; k++){
                *(*(matrizRes+i)+j) = *(*(matrizRes+i)+j) + (*(*(matriz1+i)+k) * *(*(matriz2+k)+j));
            }
        }
    }
    return matrizRes;
}

template <typename T> T **MatrizOptima<T>::multiplicarMatrices(T **matrices[], T dimMat[], int i, int j){
    if(i < j){
        return multiplicarMatriz(multiplicarMatrices(matrices,dimMat,int(i), int(*(*(this->matrizk+i)+j))),multiplicarMatrices(matrices,dimMat,int(*(*(this->matrizk+i)+j) + 1), int(j)), dimMat[i], dimMat[int(*(*(matrizk+i)+j)+1)], dimMat[j+1]);
    }
    else{
        return matrices[i];
    }
}

template <typename T> void MatrizOptima<T>::ordenDeMultiplicacion(int i, int j, string &orden){
    if (i == j){
         orden = orden + "M" + to_string(i + 1);
    }
    else{
         orden = orden + "(";
         ordenDeMultiplicacion(i,int(*(*(matrizk+i)+j)), orden);
         orden = orden + "x";
         ordenDeMultiplicacion(int(*(*(matrizk+i)+j)+1), j, orden);
         orden = orden + ")";
    }
}

template <typename T> void MatrizOptima<T>::cargarTabla(int limFila, int limCol, QTableWidget *tabla, T ** matriz){
    tabla->setRowCount(limFila);
    tabla->setColumnCount(limCol);
    QString dato;
    for (int f = 0; f < limFila; f++){
        for (int c = 0; c < limCol; c++){
            dato = (QString::number(*(*(matriz+f)+c)));
            tabla->setItem(f, c, new QTableWidgetItem(dato));
            tabla->item(f,c)->setFlags(tabla->item(f,c)->flags() & ~Qt::ItemIsEditable);
        }
    }
}

#endif // MATRIZOPTIMA_H
