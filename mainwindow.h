#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "matrizoptima.h"
#include "ayuda.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_spinBox_valueChanged(const int &arg1);

    void on_spinBox_2_valueChanged(const int &arg1);

    void on_spinBox_4_valueChanged(const int &arg1);

    void on_spinBox_6_valueChanged(const int &arg1);

    void on_spinBox_8_valueChanged(const int &arg1);

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_actionAyuda_triggered();

    void on_actionCerrar_triggered();

    void on_actionLimpiar_Pantalla_triggered();

    void closeEvent(QCloseEvent *event);

private:
    Ui::MainWindow *ui;

    MatrizOptima<float> *matriz = new MatrizOptima<float>();

    float dimMat[cantMat+1];

    float ** matrices[cantMat-1];

    float ** matrizRes;

    Ayuda *a;

};

#endif // MAINWINDOW_H
